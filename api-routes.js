// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});

// Import category controller
var categoryController = require('./Controller/categoryController');
// category routes
router.route('/category')
    .post(categoryController.index);
router.route('/category/new')
    .post(categoryController.new);

// Import Product controller
var productController = require('./Controller/productController');
// Product routes
router.route('/product')
    .post(productController.index);
router.route('/product/new')
    .post(productController.new);
router.route('/product/view')
    .post(productController.view);
router.route('/product/list/:pageno')
    .post(productController.list);



// Export API routes
module.exports = router;