// CategoryModel.js
var mongoose = require('mongoose');
// Setup schema
var categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
    parent_id: {
        type: String,
        required: true
    },
    is_parent:{
        type: Number,
        default: 0
    },
    cat_tree: {
        type: String,
        required: true
    },
   
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Category model
var Category = module.exports = mongoose.model('category', categorySchema);
module.exports.get = function (callback, limit) {
    Category.find(callback).limit(limit);
}