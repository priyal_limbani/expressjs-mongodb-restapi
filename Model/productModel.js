// ProductModel.js
var mongoose = require('mongoose');
const MongoPaging = require('mongo-cursor-pagination');
// Setup schema
var productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
   
    description: {
        type: String,
        required: true
    },
     price: {
        type: String,
        required: true
    },
    category:{
         type: String,
         required: true
    },
    slug:{
         type: String
    },
    product_tree:{
        type:String
    },
   
    productImage: { type: Object, required: true },
    create_date: {
        type: Date,
        default: Date.now
    }
});

//productSchema.plugin(MongoPaging.mongoosePlugin, { name: 'paginateFN' });
productSchema.plugin(MongoPaging.mongoosePlugin);

// Export Product model
var Product = module.exports = mongoose.model('[roduct]', productSchema);
module.exports.get = function (callback, limit) {
    Product.find(callback).limit(limit);
}