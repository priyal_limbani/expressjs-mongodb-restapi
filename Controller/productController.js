
// ProductController.js
// Import Product model
const multer = require('multer');
const path = require("path");
const MongoPaging = require('mongo-cursor-pagination');

var slug = require('slug')
Product = require('../Model/productModel');
Category = require('../Model/categoryModel');


// Handle index actions
exports.index = function (req, res) {

    var parent_id = req.body.parent_id;
    if(!parent_id){
         res.json({
                status: "error",
                message: "product parent_id required",
            });
    }
    Product.find({'parent_id':parent_id},function (err, Prosucts) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Products retrieved successfully",
            data: Prosucts
        });
    });
};
// Handle create Product actions
const storage = multer.diskStorage({
    destination: './upload/images',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1000000
    }
})


//exports.new = function(upload.single('productImage'),req, res,next) {
exports.new = [upload.array('productImage',4), function(req, res) {
//router.post("/", upload.single('productImage'), (req, res, next) => {

    if(!req.body.name){
         res.json({
                status: "error",
                message: "product name required",
            });
    }
    if(!req.body.price){
         res.json({
                status: "error",
                message: "product price required",
            });
    }
    if(!req.body.description){
         res.json({
                status: "error",
                message: "product description required",
            });
    }
    if(!req.body.category){
         res.json({
                status: "error",
                message: "product category required",
            });
    }
   /* if(!req.body.productImage){
         res.json({
                status: "error",
                message: "productImage required",
            });
    }*/
     Product.find({'name':req.body.name},function (err, productname) {
        console.log(productname)
        if(productname.length > 0){
             res.json({
                status: "error",
                message: "Duplicate Product"
            });
            // res.end();
         }        
    });
      Category.find({'name':req.body.category},function (err, Categoryname) {
        console.log(Categoryname.length);
        if(Categoryname.length == 0){
             res.json({
                status: "error",
                message: "Category not avail"
            });
            // res.end();
         }else{
            let product_tree=Categoryname[0].cat_tree;
                console.log(Categoryname[0].cat_tree);
              var filedata=req.files;
              let fileobj={};
              for (var i = 0, l = filedata.length; i < l; i++) {
                    fileobj[i]=
                        filedata[i].path;
                    
                }
                  
              const product = new Product({
                //: new mongoose.Types.ObjectId(),
                name: req.body.name,
                slug:slug(req.body.name),
                price: req.body.price,
                productImage: fileobj ,
                description:req.body.description,
                category:req.body.category,
                product_tree:product_tree
              });

              product
                .save()
                .then(result => {
                  console.log(result);
                  res.status(201).json({
                    message: "Created product successfully",
                   data: result
                  });
                })
                .catch(err => {
                  console.log(err);
                  res.status(500).json({
                    error: err
                  });
                });
            }        
    });
 
}];
exports.list = function (req, res) {
   
    Product.paginate({ limit : 2, })
        .then((result) => {
             res.json({
                status: "success",
                message: "PRoducts retrieved successfully",
                data: result
            });
        });
};
// Handle view Product
exports.view = function (req, res) {
    if(!req.body.slug){
         res.send({
                status: "error",
                message: "slug required",
            });
         res.end();
    }
    Product.find({'slug':req.body.slug},function (err, Products) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }else{           
            res.json({
                status: "success",
                message: "Categorys retrieved successfully",
                data: Products
            });
        }
    });
};