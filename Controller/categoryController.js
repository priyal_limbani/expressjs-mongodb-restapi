
// categoryController.js
// Import category model
Category = require('../Model/categoryModel');
// Handle index actions
exports.index = function (req, res) {

    if(!req.body.parent_id){
         res.send({
                status: "error",
                message: "category parent_id required",
            });
         //res.end();
    }
   
    Category.find({'parent_id':req.body.parent_id},function (err, Categorys) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Categorys retrieved successfully",
            data: Categorys
        });
    });
};
  
// Handle create Category actions
exports.new = function (req, res) {
    
     if(!req.body.name){
         res.json({
                status: "error",
                message: "category name required",
            });
        // res.end();
    }
    if(!req.body.parent_id){
         res.json({
                status: "error",
                message: "category parent_id required",
            });
        // res.end();
    }   
    Category.find({'name':req.body.name},function (err, Categoryname) {
        console.log(Categoryname)
        if(Categoryname.length > 0){
             res.send({
                status: "error",
                message: "Duplicate category"
            });
            // res.end();
         }
        
    });
    if(req.body.parent_id == 0){
        var category = new Category();
        category.is_parent=1;
        category.cat_tree=req.body.name;
        category.name = req.body.name ? req.body.name : Category.name;
        category.parent_id = req.body.parent_id;
         // save the Category and check for errors
                    category.save(function (err,datas) {
                        // Check for validation error
                        if (err)
                            res.json(err);
                        else
                            res.json({
                                message: 'New Category created!',
                                data: datas
                            });
                    });
    }else{
        //check category name avail or not
            Category.findOne({'name':req.body.parent_id},function (err, catpar) {
                if (err) {
                    res.json({
                        status: "error",
                        message: err,
                    });
                }else{
                     var category = new Category();
                //console.log(catpar.cat_tree +'>'+ req.body.name);return;
                var cattree=catpar.cat_tree +'>'+ req.body.name;
                    category.cat_tree=cattree;
                    category.name = req.body.name;
                    category.parent_id = req.body.parent_id;

                    // save the Category and check for errors
                    category.save(function (err,datas) {
                        // Check for validation error
                        if (err)
                            res.json(err);
                        else
                            res.json({
                                message: 'New Category created!',
                                data: datas
                            });
                    });
               }
            });

    }

};
// Handle view Category
exports.view = function (req, res) {
    Category.find(req.params.Category_id, function (err, Category) {
        if (err)
            res.send(err);
        res.json({
            message: 'Category details loading..',
            data: Category
        });
    });
};